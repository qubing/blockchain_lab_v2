package example03;

import org.hyperledger.fabric.contract.Context;
import org.hyperledger.fabric.contract.ContractInterface;
import org.hyperledger.fabric.contract.annotation.*;
import org.hyperledger.fabric.shim.ChaincodeException;
import org.hyperledger.fabric.shim.ChaincodeStub;

import java.nio.charset.StandardCharsets;
import java.util.Map;

/**
 * Class: MyContract
 */
@Contract(
        name = "example03.MyContract",
        info = @Info(
                title = "MyContract",
                description = "SmartContract Example 03 - Blockchain Workshop",
                version = "1.0.0",
                license = @License(
                        name = "Apache 2.0 License",
                        url = "http://www.apache.org/licenses/LICENSE-2.0.html"),
                contact = @Contact(
                        email = "23227732@qq.com",
                        name = "Bing"
                )
        )
)
@Default
public final class MyContract implements ContractInterface {

    private static String TRANSIENT_PAYMENT_JSON = "PAYMENT_JSON";

    private static String PRIVATE_COLLECTION = "PRIVATE_COLLECTION_";

    private static String EVENT_TOPUP = "TOP-UP_";

    enum Message {
        UNKNOWN_ERROR("chaincode failed with unknown reason."),
        FUNC_NOT_SUPPORT("function name '%s' is not support."),
        ARG_NUM_WRONG("Incorrect number of arguments. (Expecting %d)"),
        ACCOUNT_NOT_EXISTING("Account '%s' does not exist."),
        NO_ENOUGH_BALANCE("There is no enough balance to transfer in account '%s'."),
        BALANCE_INVALID("Account balance is invalid. ('%s': %s)"),
        LOG_STORE_PAYMENT("PrivateData Put in collection %s, content with \n |%s|\n"),
        TOPUP_COMPLETED("Account '%s' top-up completed successfully with amount of '%s'");
    
        private String tmpl;
    
        private Message(String tmpl) {
            this.tmpl = tmpl;
        }
    
        public String template() {
            return this.tmpl;
        }
    }

    /**
     * Initialize Ledger
     * @param ctx context
     */
    @Transaction(name = "Init", intent = Transaction.TYPE.SUBMIT)
    public void init(final Context ctx, final String keyA, final String valueA, final String keyB, final String valueB) {
        ChaincodeStub stub = ctx.getStub();
        try {
            Integer.valueOf(valueA);
        } catch (Exception e) {
            String errorMessage = String.format(Message.BALANCE_INVALID.template(), keyA, valueA);
            System.out.println(errorMessage);
            throw new ChaincodeException(errorMessage, e);
        }

        try {
            Integer.valueOf(valueB);
        } catch (Exception e) {
            String errorMessage = String.format(Message.BALANCE_INVALID.template(), keyB, valueB);
            System.out.println(errorMessage);
            throw new ChaincodeException(errorMessage, e);
        }

        // init account A
        stub.putStringState(keyA, valueA);
        // init account B
        stub.putStringState(keyB, valueB);
    }

    /**
     * Query Account
     * @param ctx context
     * @return name state in ledger
     */
    @Transaction(name = "Query", intent = Transaction.TYPE.EVALUATE)
    public String query(final Context ctx, final String key) {
        ChaincodeStub stub = ctx.getStub();
        String valueA = stub.getStringState(key);

        // account not existing
        if (valueA.isEmpty()) {
            String errorMessage = String.format(Message.ACCOUNT_NOT_EXISTING.template(), key);
            System.out.println(errorMessage);
            throw new ChaincodeException(errorMessage);
        }

        return valueA;
    }

    /**
     * Transfer Amount
     * @param ctx context
     */
    @Transaction(name = "Transfer", intent = Transaction.TYPE.SUBMIT)
    public void transfer(final Context ctx, final String keyFrom, final String keyTo, final String valueTrans) {
        ChaincodeStub stub = ctx.getStub();
        String valueA = stub.getStringState(keyFrom);
        String valueB = stub.getStringState(keyTo);
        int intValueA = Integer.parseInt(valueA);
        int intValueB = Integer.parseInt(valueB);
        int intValueTrans = Integer.parseInt(valueTrans);
        if (intValueA < intValueTrans) {
            String errorMessage = String.format(Message.NO_ENOUGH_BALANCE.template(), keyFrom);
            throw new ChaincodeException(errorMessage);
        }
        intValueA = intValueA - intValueTrans;
        stub.putStringState(keyFrom, String.valueOf(intValueA));
        intValueB =  intValueB + intValueTrans;
        stub.putStringState(keyTo, String.valueOf(intValueB));
    }

    /**
     * Top-up Amount
     * @param ctx context
     * @param keyTopup account to top-up
     * @param valueTopup amount to top-up
     */
    @Transaction(name = "Top-up", intent = Transaction.TYPE.SUBMIT)
    public void transfer(final Context ctx, final String keyTopup, final String valueTopup) {
        ChaincodeStub stub = ctx.getStub();

        // Get transient input and store in private data store
        Map<String, byte[]> transientMap = ctx.getStub().getTransient();
        if (transientMap != null && transientMap.containsKey(TRANSIENT_PAYMENT_JSON)) {
            byte[] paymentJSON = transientMap.get(TRANSIENT_PAYMENT_JSON);

            System.out.println(String.format(Message.LOG_STORE_PAYMENT.template(), PRIVATE_COLLECTION + stub.getMspId(), paymentJSON));
            ctx.getStub().putPrivateData(PRIVATE_COLLECTION + stub.getMspId(), TRANSIENT_PAYMENT_JSON, paymentJSON);
        } else {
            System.out.println("no transient data of payment got.");
        }

        // Top-up
        String valueA = stub.getStringState(keyTopup);
        int intValueA = Integer.parseInt(valueA);
        int intValueTopup = Integer.parseInt(valueTopup);
        intValueA = intValueA + intValueTopup;
        stub.putStringState(keyTopup, String.valueOf(intValueA));
        String msgTopup = String.format(Message.TOPUP_COMPLETED.template(), keyTopup, valueTopup);
        System.out.println(msgTopup);

        // Trigger event of 'topup'
        stub.setEvent(EVENT_TOPUP + stub.getMspId(), msgTopup.getBytes(StandardCharsets.UTF_8));
    }

    /**
     * Query Local Private Data
     * @param ctx context
     * @return name private data in local ledger
     */
    @Transaction(name = "QueryLocal", intent = Transaction.TYPE.EVALUATE)
    public String queryLocal(final Context ctx, final String key) {
        ChaincodeStub stub = ctx.getStub();
        String valuePrivate = stub.getPrivateDataUTF8(PRIVATE_COLLECTION + stub.getMspId(), key);

        return valuePrivate;
    }
}
